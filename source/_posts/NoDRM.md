---
title: NoDRM
date: 2017-06-28 21:02:48
tags:
cover: /assets/contact-bg.jpg
subtitle: Internet non ha solo cambiato radicalmente la distribuzione e la vendita al dettaglio di vestiti, libri e cd, ma ha anche sottilmente cambiato la definizione stessa del concetto di proprietà. Un qualsiasi contenuto digitale protetto con DRM potrà anche essere nel nostro computer o telefono, ma non è nostro.
---
 <h1 style="text-align: center; padding-bottom: 2em;">   No DRM Day, Giovedì 6 Luglio, Game Over Milano, ore 19.30, per più info guarda [QUI](https://wiki.fsfe.org/Events/2017/07-06-International-Day-Against-DRM)</h1>


### Quello che è mio è mio e quello che è tuo è mio

Internet non ha solo cambiato radicalmente la distribuzione e la vendita al dettaglio di vestiti, libri e cd, ma ha anche sottilmente cambiato la definizione stessa del concetto di proprietà.
Un qualsiasi contenuto digitale protetto con DRM potrà anche essere nel nostro computer o telefono, ma non è nostro: ci viene concesso di leggerlo, sentirlo o vederlo con criteri scelti unicamente da chi lo ha prodotto.
Questi criteri possono cambiare in ogni momento e senza che sia necessario alcun nostro consenso.
Benvenuti nella "RM" di DRM: Right Management (gestione dei diritti). Diritti di chi è il proprietario dei contenuti, non voi che avete comprato solamente la D di Digital.

### Diritti per chi?

La gestione dei diritti in sé non sarebbe un problema: è giusto retribuire e proteggere chi produce i contenuti. Per come è stata implementata la DRM però l'attenzione è su chi i contenuti li distribuisce, non chi li crea. Il DRM protegge il distributore e non il creatore. Il DRM massimizza i profitti del primo, relegando l'altro ad un ruolo secondario.

### Internet città aperta

Uno dei fattori chiave del successo di internet risiede nella adozione di standard aperti a tutti, trasparenti, liberi da brevetto e implementabili da chiunque senza pagamento di royalties. Inevitabilmente un meccanismo DRM deve essere (almeno in parte) gestito in maniera chiusa, proprietaria. Considerati i potenziali impatti, questa mancanza di trasparenza può rendere più difficile la crescita di Internet come "collante" globale: certi contenuti potrebbero essere fruibili solo con certi programmi e non altri, i programmi e moduli "chiusi" potrebbero contenere funzioni non desiderate se non addirittura illegali, lesive della privacy od eticamente discutibili.

### Come funziona HTML5/DRM

Dato che i contenuti digitali visibili in un browser sono per loro natura variabili in funzione di chi controlla il sito che li produce, HTML5/DRM prevede alla base che chi voglia proteggere i propri contenuti installi un modulo proprietario all'interno del browser compatibile che ne controlli la visualizzazione.
Lo standard è estremamente generico sul tipo di funzionalità che un modulo di protezione possa offrire proprio perché i vari Netflix, Youtube e SnapChat vogliono avere le mani libere di implementare le tipologie di controllo più redditive.
Tra le funzioni più gettonate:

  - encryption: se un utente intercetta il flusso video in streaming per salvarlo su disco non potrà vederlo.
  - zone enforcement: per questioni contrattuali certe trasmissioni debbono essere visibili solo in certi stati e non altri
  - billing: richiedere un pagamento in funzione di quanto/quante volte l'utente accede ad un certo contenuto

Tutti i maggiori browser supportano HTML5/DRM: per ora gli utilizzatori sono molto pochi, ma cresceranno.

### Cosa potrebbe non funzionare

In generale si può dire "oggi posso vedere un film, domani forse". Ogni volta che si accede un contentuto protetto il modulo controlla se l'utente lo può accedere.
Alcuni moduli se non sono connessi ad internet rifiuteranno la richiesta (dipende da chi ha scritto il modulo). Se ad esempio l'azienda che distribuisce i contenuti chiude, non è affatto detto che chi ha regolarmente pagato il contenuto ne possa ancora fruire. Ma è anche del tuto possibile che col tempo chi distribuisce il contenuto cambi le TOS (termini d'uso) e con esse se un certo utente possa accedere o meno al contenuto regolarmente pagato. Un altro rischio è la balcanizzazione: se un certo contenuto viene venduto da entità differenti (con moduli differenti sul browser) ci potrebbero essere problemi di fruizione dell'utente che scelga di vedere lo stesso contenuto da fonti diverse.
Se molte entità decidono di buttarsi in questo business il numero di moduli da installare diventerà elevato, e con essi i problemi associati.
Dove c'è il software, c'è il bug, ricordatevelo.
